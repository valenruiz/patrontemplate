public abstract class Pago  implements Crud{

    @Override
    public abstract void insert(String s);

    @Override
    public abstract void update(String s);

    @Override
    public abstract void select();

    @Override
    public abstract void delete(String s);
    
}