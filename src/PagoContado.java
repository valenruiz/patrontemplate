public class PagoContado  extends Pago {

    @Override
    public void insert(String s) {
        System.out.println("Se insertó el pago de contado por $1000");
    }

    @Override
    public void update(String s) {
        System.out.println("Se actualizo el pago de contado por $500");
        
    }

    @Override
    public void select() {
        for (int i = 1; i <= 10; i++) {
            System.out.println("Pago de contado " + i + " por valor de " + i*100);
        }
        
    }

    @Override
    public void delete(String s) {
        System.out.println("Eliminado el pago de contado " + s);
    }
    
}
