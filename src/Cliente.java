public class Cliente implements Crud {

    @Override
    public void insert(String s) {
        System.out.println("Insertado el cliente " + s);
        
    }

    @Override
    public void update(String s) {
        System.out.println("Actualizado el cliente " + s);
        
    }

    @Override
    public void select() {
        System.out.println("Listado de clientes");
        for (int i = 1; i <= 10; i++) {
            System.out.println("Cliente " + i + " : C" + i);
        }
        
    }

    @Override
    public void delete(String s) {
        System.out.println("Eliminado el cliente " + s);
    }
    
}
