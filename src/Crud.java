public interface Crud {

    /**
     * Plantilla con los métodos crud
     */

     public void insert(String s);

     public void update(String s);

     public void select();

     public void delete(String s);
    
}
/*
     * extiende determinados comportamientos dentro de un mismo algoritmo por parte de 
     * diferentes entidades. Es decir, diferentes entidades tienen un comportamiento similar 
     * pero que difiere en determinados aspectos puntuales en función de la entidad concreta. 
*/