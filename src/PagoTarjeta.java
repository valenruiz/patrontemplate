public class PagoTarjeta  extends Pago {

    @Override
    public void insert(String s) {
        System.out.println("Se insertó el pago con tarjeta Visa por $1000");
    }

    @Override
    public void update(String s) {
        System.out.println("Se actualizo el pago con tarjeta Visa por $500");
        
    }

    @Override
    public void select() {
        for (int i = 1; i <= 10; i++) {
            System.out.println("pago con tarjeta Visa " + i + " por valor de " + i*100);
        }
        
    }

    @Override
    public void delete(String s) {
        System.out.println("Eliminado el pago con tarjeta Visa " + s);
    }
    
}
